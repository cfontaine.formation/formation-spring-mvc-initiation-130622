package fr.dawan.springcore;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;

import fr.dawan.springcore.beans.Adresse;
import fr.dawan.springcore.beans.Client;
import fr.dawan.springcore.componants.UserDao;

public class AppJava {

    public static void main(String[] args) {
        // Création du conteneur
        ApplicationContext ctx = new AnnotationConfigApplicationContext(AppConfig.class);
        System.out.println("-------------------------------");
        
        // getBean permet de récupérer les instances des beans depuis le conteneur
        Adresse adr1 = ctx.getBean("adresse1", Adresse.class);
        System.out.println(adr1);
        
        UserDao dao = ctx.getBean("userDao", UserDao.class);
        System.out.println(dao);

        Client clA = ctx.getBean("clientA", Client.class);
        System.out.println(clA);

        Client cl2 = ctx.getBean("client2", Client.class);
        System.out.println(cl2);

        System.out.println("-------------------------------");
        ((AbstractApplicationContext) ctx).close();
    }

}
