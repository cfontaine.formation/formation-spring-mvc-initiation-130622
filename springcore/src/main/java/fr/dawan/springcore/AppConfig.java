package fr.dawan.springcore;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Description;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;

import fr.dawan.springcore.beans.Adresse;
import fr.dawan.springcore.beans.Client;

// Configuration avec java (à priviligier avec spring boot)
@Configuration // annotation pour indiquer que la classe JavaConfig est une classe de configuration
// @Lazy // => retarder la création de tous les beans singleton de la classe à la première utilisation (lazy loading) 
@ComponentScan(basePackages = "fr.dawan.springcore") // choix du package à scanner pour découvrir des composants
public class AppConfig {

    // @Bean -> Déclaration d'un bean
    // le nom du bean (id,name en xml) = le nom de la méthode, on peut aussi le préciser avec l'attribut name de @Bean
    // La classe (class en xml) du bean correspont au type de retour de la méthode
    @Bean
    public Adresse adresse1() {
        return new Adresse("1 rue esquermoise", "Lille", "59800");
    }

    @Bean
    @Description("Un Bean Adresse")
    public Adresse adresse2() {
        return new Adresse();
    }

    // On peut matérialiser les dépendances avec les paramètres de la méthode
    // ici lien explicite avec le bean qui pour nom adresse1 (équivalent à ref en xml)
    @Bean(name = "clientA")
    @Lazy // => lazy loading uniquement pour ce bean
    public Client client1(Adresse adresse1) {
        return new Client("John", adresse1, "Do", "1234-567");
    }

    @Bean(initMethod = "init", destroyMethod = "destroy") // initMethod et destroyMethod => Cycle de vie des beans
    @Scope("prototype") // Modification de la portée du beans en prototype (par défaut singleton)
    public Client client2() {
        return new Client("John", adresse2(), "Do", "1234-567");
    }
}
