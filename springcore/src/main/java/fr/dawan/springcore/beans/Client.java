package fr.dawan.springcore.beans;

public class Client extends Personne {

    private static final long serialVersionUID = 1L;

    private String numClient;

    public Client() {
        super();
    }

    public Client(String prenom, Adresse adresse, String nom,String numClient) {
        super(prenom, adresse, nom);
        this.numClient=numClient;
    }

    public String getNumClient() {
        return numClient;
    }

    public void setNumClient(String numClient) {
        this.numClient = numClient;
    }

    @Override
    public String toString() {
        return "Client [numClient=" + numClient + ", toString()=" + super.toString() + "]";
    }

    public void init() {
        System.out.println("Méthode init");
    }

    public void destroy() {
        System.out.println("Méthode destroy");
    }
}
