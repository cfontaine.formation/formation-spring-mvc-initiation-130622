package fr.dawan.springcore.beans;

import java.io.Serializable;
import java.util.List;

public class Personne implements Serializable {

    private static final long serialVersionUID = 1L;
    
    private String prenom;
    
    private String nom;
    
    private Adresse adresse;
    
    private List<Telephone > telephones;

    public Personne() {
        super();
    }

    public Personne(String prenom, Adresse adresse, String nom) {
        super();
        this.prenom = prenom;
        this.nom = nom;
        this.adresse = adresse;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Adresse getAdresse() {
        return adresse;
    }

    public void setAdresse(Adresse adresse) {
        this.adresse = adresse;
    }
    
    public List<Telephone> getTelephones() {
        return telephones;
    }

    public void setTelephones(List<Telephone> telephones) {
        this.telephones = telephones;
    }

    @Override
    public String toString() {
        return "Personne [prenom=" + prenom + ", nom=" + nom + ", adresse=" + adresse + "]";
    }
}
