package fr.dawan.springcore.beans;

import java.io.Serializable;

public class Telephone implements Serializable{

    private static final long serialVersionUID = 1L;
    
    private String numero;

    public Telephone() {
        super();
    }

    public Telephone(String numero) {
        super();
        this.numero = numero;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    @Override
    public String toString() {
        return "Telephone [numero=" + numero + "]";
    }
}