package fr.dawan.springcore.beans;

import java.io.Serializable;

public class Contact implements Serializable{

    private static final long serialVersionUID = 1L;

    private String prenom;
    
    private String nom;
    
    private Adresse adresse;
    
    private String email;

    public Contact() {
        super();
    }

    public Contact(String prenom, String nom, Adresse adresse, String email) {
        super();
        this.prenom = prenom;
        this.nom = nom;
        this.adresse = adresse;
        this.email = email;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Adresse getAdresse() {
        return adresse;
    }

    public void setAdresse(Adresse adresse) {
        this.adresse = adresse;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "Contact [prenom=" + prenom + ", nom=" + nom + ", adresse=" + adresse + ", email=" + email + "]";
    }
    
    
}
