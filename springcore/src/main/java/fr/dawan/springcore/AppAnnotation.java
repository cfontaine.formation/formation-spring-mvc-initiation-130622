package fr.dawan.springcore;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import fr.dawan.springcore.componants.UserDao;
import fr.dawan.springcore.componants.UserService;

public class AppAnnotation {

    public static void main(String[] args) {
        ApplicationContext context=new ClassPathXmlApplicationContext("annotation.xml");
        System.out.println("---------------------------------------");
        UserDao dao=context.getBean("userDao",UserDao.class);
        
        System.out.println(dao);
        System.out.println(dao.getVal());
        
        UserService service=context.getBean("userService",UserService.class);
        System.out.println(service);
        
        UserService service2=context.getBean("userService",UserService.class);
        System.out.println(service2);
        
        System.out.println("---------------------------------------");
        ((AbstractApplicationContext)context).close();
    }

}
