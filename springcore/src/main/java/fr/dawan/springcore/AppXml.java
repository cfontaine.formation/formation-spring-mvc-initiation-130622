package fr.dawan.springcore;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import fr.dawan.springcore.beans.Adresse;
import fr.dawan.springcore.beans.Client;
import fr.dawan.springcore.beans.Contact;
import fr.dawan.springcore.beans.Formation;
import fr.dawan.springcore.beans.Personne;

public class AppXml 
{
    public static void main( String[] args )
    {
        // Création du conteneur
        ApplicationContext ctx=new ClassPathXmlApplicationContext( new String[] {"beans.xml","adresse.xml"});
        System.out.println("------------------------------------------------------------");
        
        // getBean => permet de récupérer les instances des beans depuis le conteneur
        
        // formation1 => prototype f1 et f4 sont des objets diférents 
        Formation f1=ctx.getBean("formation1",Formation.class);
        System.out.println(f1);
        
        Formation f4=ctx.getBean("formation1",Formation.class);
        System.out.println(f4);
        
        // formation2=> singleton f2 et f3 même objet
        Formation f2=ctx.getBean("formation2",Formation.class);
        System.out.println(f2);
        
        Formation f3=ctx.getBean("formationWord",Formation.class);
        System.out.println(f3);
        
        // Exercice Adresse
        Adresse adresse1=ctx.getBean("adresse",Adresse.class);
        System.out.println(adresse1);
        
//        Adresse adresse2=ctx.getBean("adresseLille",Adresse.class);
//        System.out.println(adresse2);
        
        // Injection explicite des dépendances
        Personne p1=ctx.getBean("per1",Personne.class);
        System.out.println(p1);
        
        Personne p1a=ctx.getBean("per1a",Personne.class);
        System.out.println(p1a);
        
        Personne p1b=ctx.getBean("per1b",Personne.class);
        System.out.println(p1b);
        
        // Injection automatique des dépendances (autowiring)
        // autowire par type
        Personne p2=ctx.getBean("per2",Personne.class);
        System.out.println(p2);
        
        // autowire par nom 
        Personne p3=ctx.getBean("per3",Personne.class);
        System.out.println(p3);
        
        // autowire par constructeur
        Personne p4=ctx.getBean("per4",Personne.class); 
        System.out.println(p4);
        
        // Héritage de configuration
        Client cl1=ctx.getBean("client1",Client.class);
        System.out.println(cl1);
        
        // Redéfinition d'attribut
        Client cl2=ctx.getBean("client2",Client.class);
        System.out.println(cl2);
        
        // 
        Contact ct1=ctx.getBean("contact1",Contact.class);
        System.out.println(ct1);
        
        //  Héritage de configuration sans instanciation du parent
        // avec L’attribut abstract sur l’élément <bean> il ne peut être instancié
//        Personne pAbst=ctx.getBean("perAbst",Personne.class);
//        System.out.println(pAbst);
        Client cl3=ctx.getBean("client3",Client.class);
        System.out.println(cl3);
        
        // Collection
        Personne p6=ctx.getBean("per6",Personne.class);
        System.out.println(p6);
        p6.getTelephones().forEach(System.out::println);
        
        Client cl4=ctx.getBean("client4",Client.class);
        System.out.println(cl4);
        cl4.getTelephones().forEach(System.out::println);
        
        // Fermer le conteneur
        ((AbstractApplicationContext)ctx).close();
    }
}
