package fr.dawan.springcore.componants;

import javax.inject.Inject;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

@Service(value="userService")
@Scope("prototype")
public class UserService {
    
    //@Autowired
    private UserDao dao;

        
    public UserService() {
        super();
    }

   // @Autowired(required = false)
    @Inject
    public UserService(UserDao dao) {
        this.dao = dao;
    }

    public UserDao getDao() {
        return dao;
    }

    //@Autowired
    public void setDao(UserDao dao) {
        this.dao = dao;
    }

    @Override
    public String toString() {
        return "UserService [dao=" + dao + ", toString()=" + super.toString() + "]";
    }


}
