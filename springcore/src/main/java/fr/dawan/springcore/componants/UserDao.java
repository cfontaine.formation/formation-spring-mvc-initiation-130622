package fr.dawan.springcore.componants;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Repository;

import fr.dawan.springcore.beans.Adresse;

@Repository(value = "userDao")
@PropertySource("classpath:application.properties")
@Lazy
public class UserDao {

    @Resource(name = "adresse2")
//    @Autowired
//    @Qualifier(value = "adresse2")
    private Adresse data;

    @Value("${hello}")
    private String val;

    public UserDao() {
        super();
    }

    /* @Autowired */
    public UserDao(/* @Qualifier(value="adresse") */ Adresse data) {
        super();
        this.data = data;
    }

    public Adresse getData() {
        return data;
    }

    /* @Autowired */
    public void setData(/* @Qualifier(value="adresse") */Adresse data) {
        this.data = data;
    }

    public String getVal() {
        return val;
    }

    public void setVal(String val) {
        this.val = val;
    }

    @Override
    public String toString() {
        return "UserDao [data=" + data + "]";
    }

    @PostConstruct
    public void init() {
        System.out.println("Méthode init");
    }

    @PreDestroy
    public void detruire() {
        System.out.println("Méthode detruire");
    }

}
