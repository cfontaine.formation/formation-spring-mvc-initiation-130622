package fr.dawan.springmvc.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import fr.dawan.springmvc.beans.User;

@Component
public class FakeUserDao {

    private static List<User> users = new ArrayList<>();

    private static long nextId = 4;

    static {
        User u = new User("John", "Doe", "jd@dawan.com", "123456");
        u.setId(1L);
        users.add(u);
        u = new User("Jane", "Doe", "jad@dawan.com", "123456");
        u.setId(2L);
        users.add(u);
        u = new User("Alan", "Smithee", "asmithee@dawan.com", "123456");
        u.setId(3L);
        users.add(u);
        u = new User("Dan", "Cooper", "dc@dawan.com", "123456");
        u.setId(4L);
        users.add(u);
    }

    public void saveOrUpdate(User u) {
        if (u.getId() != 0) {
            for (int i = 0; i < users.size(); i++) {
                if (users.get(i).getId() == u.getId()) {
                    users.set(i, u);
                }
            }
        } else {
            u.setId(++nextId);
            users.add(u);
        }
    }

    public void delete(long id) {
        users.remove(findByID(id));
    }
    
    public void delete(User u) {
        users.remove(u);
    }

    public List<User> findAll() {
        return users;
    }

    public User findByID(long id) {
        for (User u : users) {
            if (u.getId() == id) {
                return u;
            }
        }
        return null;
    }
    
    public User findByEmail(String email) {
        for (User u : users) {
            if (u.getEmail() == email) {
                return u;
            }
        }
        return null;
    }

    public User findByEmailAndPassword(String email, String password) {
        for (User u : users) {
            if (u.getPassword().equals(password) && u.getEmail().equals(email)) {
                return u;
            }
        }
        return null;
    }
}
