package fr.dawan.springmvc.controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import fr.dawan.springmvc.beans.User;
import fr.dawan.springmvc.dao.FakeUserDao;
import fr.dawan.springmvc.forms.UserForm;

@Controller
@RequestMapping("/admin")
public class AdminController {
    
    @Autowired
    FakeUserDao dao;

    @GetMapping("/users")
    public String affUtilisateurs(Model model) {
        List<User> lstUser=dao.findAll();
        model.addAttribute("users",lstUser);
        return "utilisateur";
    }
    
    @GetMapping("/users/delete/{id}")
    public String deleteUser(@PathVariable long id) {
        try {
            dao.delete(id);
        } catch (Exception e) {

        }
        return "redirect:/admin/users";
    }
    
    
    @GetMapping("/users/add")
    public String addUser(@ModelAttribute("formUser") UserForm formUser) {
        return "ajoututilisateur";
    }

    @PostMapping("/users/add")
    public String addUser(@Valid @ModelAttribute("formUser") UserForm formUser,BindingResult result,Model model) {
        if(result.hasErrors()) {
            model.addAttribute("errors",result);
            model.addAttribute("formuser", formUser);
            return "ajoututilisateur";
        }
        if(dao.findByEmail(formUser.getEmail())!=null) {
            model.addAttribute("formuser", new UserForm());
            model.addAttribute("msgerr", "L'utilisateur existe déjà");
            return "ajoututilisateur";
        }
        
        User user=new User(formUser.getPrenom(),formUser.getNom(),formUser.getEmail(),formUser.getPassword());
        dao.saveOrUpdate(user);
        return "redirect:/admin/users";
    }

}
