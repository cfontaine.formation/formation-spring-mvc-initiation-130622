package fr.dawan.springmvc.controllers;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import fr.dawan.springmvc.beans.User;
import fr.dawan.springmvc.dao.FakeUserDao;
import fr.dawan.springmvc.forms.LoginForm;

@Controller
@SessionAttributes("isConnected")
public class LoginController {

    @Autowired
    FakeUserDao dao;

    @GetMapping("/login")
    public String loginget(@ModelAttribute("loginform") LoginForm form) {
        return "login";
    }

    @PostMapping("/login")
    public ModelAndView loginPost(@Valid @ModelAttribute("loginform") LoginForm form, BindingResult results,@ModelAttribute("isConnected") Boolean isConnected) {

        ModelAndView mdv = new ModelAndView();
        if (results.hasErrors()) {
            mdv.setViewName("login");
            mdv.addObject("loginform", form);
            mdv.addObject("errors", results);
        } else {
            User u = dao.findByEmailAndPassword(form.getEmail(), form.getPassword());
            if (u != null) {
                mdv.addObject("isConnected", true);
                mdv.setViewName("redirect:/admin/users");
            }
            else {
                mdv.setViewName("redirect:/login");
            }
        }
        return mdv;
    }
    
    @GetMapping("/logout")
    public String logout(@ModelAttribute ("isConnected") Boolean connected,Model model )
    {
        model.addAttribute("isConnected",false);
        return"redirect:/exemple";
    }


    @ModelAttribute("isConnected")
    public Boolean getIsConnected() {
        return false;
    }

}
