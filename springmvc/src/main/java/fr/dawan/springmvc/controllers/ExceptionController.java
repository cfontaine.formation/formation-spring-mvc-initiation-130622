package fr.dawan.springmvc.controllers;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ExceptionController {
    
    @ExceptionHandler
    public String gestionException(Exception exp,Model model) {
        model.addAttribute("msgExp", "Controlleur exception: " +exp.getMessage());
        model.addAttribute("trace",exp.getStackTrace());
        return "exception";
    }

}
