package fr.dawan.springmvc.controllers;

import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

import javax.inject.Provider;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import fr.dawan.springmvc.beans.Personne;
import fr.dawan.springmvc.beans.User;

@Controller
@SessionAttributes({ "user2", "user3" })
@RequestMapping("/exemple")
public class ExempleController {

    private static int cpt;

    // Provider session methode 3
    @Autowired
    private Provider<User> provider;

    // @RequestMapping(value = "/helloworld", "/hello",method = RequestMethod.GET)
    @GetMapping(value = { "/helloworld", "/hello" })
    public String helloWorld() {
        return "helloWorld"; // une méthode du controleur retourne le nom de la vue
    }

    @GetMapping("/testmodel")
    public String testModel(Model model) {
        model.addAttribute("msg", "Test Model");
        return "exemple";
    }

    @GetMapping("/testmodelandview")
    public ModelAndView testModelAndView() {
        ModelAndView mdv = new ModelAndView();
        mdv.setViewName("exemple");
        mdv.addObject("msg", "Test ModelAndView");
        return mdv;
    }

    @GetMapping(value = "/testparams", params = "id=42")
    public String testParams(Model model) {
        model.addAttribute("msg", "Le paramètre id est présent et il est égal à 42");
        return "exemple";
    }

    @GetMapping("/testpath/{id}")
    public String testPathVariable(@PathVariable String id, Model model) {
        model.addAttribute("msg", "L'url contient un paramètre id= " + id);
        return "exemple";
    }

    @GetMapping("/testpath/{id}/action/{action}")
    public String testPathVariable(@PathVariable String id, @PathVariable String action, Model model) {
        model.addAttribute("msg", "L'url contient un paramètre id= " + id + " action= " + action);
        return "exemple";
    }

    @GetMapping("/testpathambig/{id:[0-9]+}")
    public String testPathVariableAmbigI(@PathVariable String id, Model model) {
        model.addAttribute("msg", "L'url contient un paramètre id= " + id);
        return "exemple";
    }

    @GetMapping("/testpathambig/{name:[a-zA-Z]+}")
    public String testPathVariableAmbigN(@PathVariable String name, Model model) {
        model.addAttribute("msg", "L'url contient un paramètre name= " + name);
        return "exemple";
    }

    @GetMapping(value = { "/testpathoption/{id}", "/testpathoption" })
    public String testPathVariableOptionnel(@PathVariable(required = false) String id, Model model) {
        model.addAttribute("msg", "@PathVariable " + id);
        return "exemple";
    }

    @GetMapping("/testparam")
    public String testParam(@RequestParam String id, Model model) {
        model.addAttribute("msg", "@RequestParam " + id);
        return "exemple";
    }

    @GetMapping("/testparamdefault")
    public String testParamDefault(@RequestParam(defaultValue = "0") String id, Model model) {
        model.addAttribute("msg", "@RequestParam  (valeur par défaut) " + id);
        return "exemple";
    }

    @GetMapping("/testparamconv")
    public String testParam(@RequestParam int id, Model model) {
        model.addAttribute("msg", "@RequestParam (convertion int) " + id);
        return "exemple";
    }

    @GetMapping("/testparamdate")
    public String testParam(@RequestParam @DateTimeFormat(pattern = "dd-MM-yyyy") LocalDate date, Model model) {
        model.addAttribute("msg", "@RequestParam (convertion date) " + date);
        return "exemple";
    }

    @GetMapping("/testheader")
    public String testHeader(@RequestHeader("User-Agent") String userAgent, Model model) {
        model.addAttribute("msg", "@RequestHeader=" + userAgent);
        return "exemple";
    }

    @GetMapping("/testallheader")
    public String testHeader(@RequestHeader HttpHeaders httpHeaders, Model model) {
        List<String> lst = new ArrayList<>();
        Set<Entry<String, List<String>>> sent = httpHeaders.entrySet();
        for (Entry<String, List<String>> e : sent) {
            String s = e.getKey() + " = ";
            for (String el : e.getValue()) {
                s += el + " ";
            }
            lst.add(s);
        }
        model.addAttribute("lstHeader", lst);
        return "exemple";
    }

    @GetMapping("/testredirect")
    public String testRedirect() {
        return "redirect:/exemple/helloworld";
    }

    @GetMapping("/testforward")
    public String testForward() {
        return "forward:/exemple/helloworld";
    }

//    @ModelAttribute("compteur")
//    public int testModelAttribute() {
//        return cpt++;
//    }
//    
    @ModelAttribute
    public void testModelAttribute(Model model) {
        model.addAttribute("compteur", cpt++);
    }

    @ModelAttribute("per1")
    public Personne testModelAttribute() {
        return new Personne("alan", "smithee");
    }

    @GetMapping("/testmodelattr")
    public String testModelAttrParam(@ModelAttribute("per1") Personne p, Model model) {
        model.addAttribute("msg", p);
        return "exemple";
    }

    @GetMapping("")
    public String exemple(Model model) {
        return "exemple";
    }

    // flashattribute
    @GetMapping("/testflash")
    public String testFlashAttribute(RedirectAttributes r) {
        r.addFlashAttribute("msgFlash", "Vous venez d'être rediriger");
        return "redirect:/exemple/cibleflash";
    }

    @GetMapping("/cibleflash")
    public String cibleFlashAttribute(@ModelAttribute("msgFlash") String msgFlash, Model model) {
        model.addAttribute("msg", msgFlash);
        return "exemple";
    }

    // Exception
    @GetMapping("/genioexception")
    public void genIoException() throws IOException {
        throw new IOException("Le fichier n'existe pas");
    }

    @GetMapping("/gensqlexception")
    public void genSQLException() throws SQLException {
        throw new SQLException("La requête SQL est bancale");
    }

    @ExceptionHandler(IOException.class)
    public String gestionIoException(Exception e, Model model) {
        model.addAttribute("msgExp", e.getMessage());
        model.addAttribute("trace", e.getStackTrace());
        return "exception";
    }

    // gestion =>  Page d'erreur
    @GetMapping("/erreur")
    public String pageError(HttpServletRequest request, Model model) {
        int codeErreur = (int) request.getAttribute("javax.servlet.error.status_code");
        switch (codeErreur) {
        case 404:
            model.addAttribute("msgErreur", "La page est introuvable");
            break;
        case 500:
            model.addAttribute("msgErreur", "Un erreur interne c'est produite");
            break;
        case 403:
            model.addAttribute("msgErreur", "Vous n'avez pas l'autorisation de consulter cette page");
            break;
        default:
            model.addAttribute("msgErreur", "Un erreur c'est produite");
        }
        model.addAttribute("codeErreur", codeErreur);
        return "erreur";
    }

    // Cookie
    @GetMapping("/writecookie")
    public String writeCookie(HttpServletResponse response, RedirectAttributes ra) {
        Cookie c = new Cookie("testCookie", "ValeurDeTest");
        c.setMaxAge(30);
        response.addCookie(c);
        ra.addFlashAttribute("msgFlash", "Le cookie testCookie a été créé avec la valeur: ValeuDeTest");
        return "redirect:/exemple/cibleflash";
    }

    @GetMapping("/readcookie")
    public String readCookie(@CookieValue(name = "testCookie", defaultValue = "absent") String value, Model model) {
        model.addAttribute("msg", value);
        return "exemple";
    }

    // Session
    // 1 => HttpServletRequest (JavaEE)
    @GetMapping("/testsession1")
    public String testSession1(HttpServletRequest request, Model model) {
        HttpSession session = request.getSession();
        session.setAttribute("user1", new User("John", "Doe", "jd@dawan.com", "123456"));
        model.addAttribute("msg", "Ajout utilisateur dans la session(HttpSession)");
        return "exemple";
    }

    @GetMapping("/readsession1")
    public String readSession1(HttpServletRequest request, Model model) {
        HttpSession session = request.getSession();
        User u = (User) session.getAttribute("user1");
        if (u != null) {
            model.addAttribute("msg", "Session 1: " + u);
        }
        return "exemple";
    }

    // 2 -> @SessionAttribute, @ModelAttribute
    @GetMapping("/testsession2")
    public String testSession2(@ModelAttribute("user2") User user, Model model) {
        model.addAttribute("user2", new User("Jane", "Doe", "jd@dawan.com", "azerty"));
        model.addAttribute("msg", "Ajout utilisateur dans la session( @SessionAttribute, @ModelAttribute");
        return "exemple";
    }

    @GetMapping("/readsession2")
    public String readSession2(@ModelAttribute("user2") User user, Model model) {
        if (user != null) {
            model.addAttribute("msg", "Session 2: " + user);
        }
        return "exemple";
    }

    @ModelAttribute("user2")
    public User getRequestUser2() {
        return new User();
    }

    // 3 -> javax.inject Provider
    @GetMapping("/testsession3")
    public String testSession3(Model model) {
        User u = provider.get();
        u.setPrenom("Alan");
        u.setNom("Smithee");
        u.setEmail("al@dawan.com");
        u.setPassword("123456");
        model.addAttribute("msg", "Ajout utilisateur dans la session(java.Inject)");
        return "exemple";
    }

    @GetMapping("/readsession3")
    public String readSession3(Model model) {
        User user = provider.get();
        if (user != null) {
            model.addAttribute("msg", "Session 2: " + user);
        }
        return "exemple";
    }
}
