package fr.dawan.springmvc.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import fr.dawan.springmvc.beans.Personne;

@Controller
@RequestMapping("/presentation")
public class PresentationController {

    @GetMapping("")
    public String presentation() {
        return "presentation";
    }

    @GetMapping("/path/{prenom}/{nom}")
    public String presentationPath(@PathVariable String prenom, @PathVariable String nom, Model model) {
        model.addAttribute("prenom", prenom);
        model.addAttribute("nom", nom);
        return "presentation";
    }

    @GetMapping("/param")
    public String presentationParam(@RequestParam String prenom, @RequestParam String nom, Model model) {
        model.addAttribute("prenom", prenom);
        model.addAttribute("nom", nom);
        return "presentation";
    }
    
    @GetMapping("/pathb/{prenom}/{nom}")
    public String presentationPath(Personne personne, Model model) {
        model.addAttribute("personne", personne);
        return "presentation";
    }

    @GetMapping("/paramb")
    public String presentationParam(Personne personne, Model model) {
        model.addAttribute("personne", personne);
        return "presentation";
    }
}
