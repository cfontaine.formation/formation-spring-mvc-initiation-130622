<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:import url="header.jsp">
    <c:param name="titre" value="presention"/>
</c:import>
<h1>Presentation</h1>
<c:if test="${! empty prenom and !empty nom}">
    <div class="alert alert-primary col-md-6 my-5">Bonjour, <c:out value="${prenom} ${nom}"/></div>
</c:if>

<c:if test="${! empty personne}">
    <div class="alert alert-secondary col-md-6 my-5">Bonjour, <c:out value="${personne.prenom} ${personne.nom}"/></div>
</c:if>

<div class="my-4">
<a href="<c:url value='/presentation/path/john/doe' context='/springmvc'/>">Salutation (path)</a>
</div>
<div class="my-4">
<a href="<c:url value='/presentation/param?prenom=jane&nom=doe' context='/springmvc'/>">Salutation (param) </a>
</div>
<h3>Lien bean</h3>
<div class="my-4">
<a href="<c:url value='/presentation/pathb/john/doe' context='/springmvc'/>">Salutation (path)</a>
</div>
<div class="my-4">
<a href="<c:url value='/presentation/paramb?prenom=jane&nom=doe' context='/springmvc'/>">Salutation (param) </a>
</div>

<c:import url="footer.jsp"/>