<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%> 
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
      
<c:import url="header.jsp">
    <c:param name="titre" value="Inscription"/>
</c:import>


<c:url value="/users/add" context="/springmvc/admin" var="urladd"/>
<form:form method="post" action="${urladd}"  modelAttribute="formUser">

<div class="form-group">
    <div class="col-md-8 mt-3">
        <form:label path="prenom">Prénom</form:label>
        <form:input class="form-control" path="prenom" placeholder="Entrer votre prenom"/>
        <form:errors class="small text-danger" path="prenom"/>
    </div>
    
    <div class="col-md-8 mt-3">
        <form:label path="nom">Nom</form:label>
        <form:input class="form-control" path="nom" placeholder="Entrer votre nom"/>
        <form:errors class="small text-danger" path="nom"/>
    </div>

    <div class="col-md-8 mt-3">
        <form:label path="email">Email</form:label>
        <form:input type="email" class="form-control" path="email" placeholder="Entrer votre email"/>
        <form:errors class="small text-danger" path="email"/>
    </div>
    
    <div class="col-md-8 mt-3">
        <form:label path="password">Mot de passe</form:label>
        <form:password class="form-control" path="password" placeholder="Entrer votre mot de passe"/>
        <form:errors class="small text-danger" path="password"/>
    </div>
    
    <div class="col-md-8 mt-3">
        <input class="btn btn-primary" type="submit" value="Ajouter"/>
    </div>
</div>
</form:form>

<c:if test="${ !empty msgerr}">
<div class="alert alert-danger"><c:out value="${msgerr}"/></div>
</c:if>
<c:import url="footer.jsp"/>
