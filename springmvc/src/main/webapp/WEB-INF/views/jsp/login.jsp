<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<c:import url="header.jsp">
    <c:param name="titre" value="Login"/>
</c:import>

<h1>Login</h1>
<c:url value='/login' context='/springmvc' var="urllogin"/>
<form:form method="post" action="${urlLogin}" modelAttribute="loginform">
  <div class="mb-3">
    <form:label class="form-label" path="email">Email</form:label>
    <form:input class="form-control" path="email" type="text"/>
    <form:errors class="text-danger small" path="email"/>
  </div>
  <div class="mb-3">
    <form:label class="form-label" path="password">Password</form:label>
    <form:input class="form-control" path="password" type="password"/>
    <form:errors class="text-danger small" path="password"/>
  </div>
   <input class="btn btn-primary" type="submit" value="Connexion"/>
</form:form>
<c:import url="footer.jsp"/>