<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <c:url value="/webjars/bootstrap/5.1.3/css/bootstrap.min.css" context="/springmvc" var="urlbootstrap"/>
    <link rel="stylesheet" href="${urlbootstrap}"/>
    <title><c:out value='${empty param.titre?"springmvc":param.titre }'/></title>
</head>
<body >

<nav class="navbar navbar-expand-lg navbar-dark bg-dark sticky-top">
  <div class="container-fluid">
    <a class="navbar-brand" href="<c:url value="/exemple" context="/springmvc" />">Formation spring</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarToggler" aria-controls="navbarToggle" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarToggler">
        <ul class="navbar-nav" >
        <li class="nav-item">
            <a class="nav-link" href="<c:url value="/exemple" context="/springmvc" />">Exemple</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href='<c:url value="/presentation" context="/springmvc"/>'>Présentation</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href='<c:url value="/admin/users" context="/springmvc"/>'>Utilisateurs</a>
        </li>
        </ul>
    </div>
    
   <c:choose>
      <c:when test='${empty sessionScope.isConnected ||  not sessionScope.isConnected}'>
       <a class="btn  btn-success btn-sm" href="<c:url value='/login' context='/springmvc'/>" >Login</a>
      </c:when>
      <c:otherwise>
          <a class="btn  btn-secondary btn-sm" href="<c:url value='/logout' context='/springmvc'/>" >Logout</a>
      </c:otherwise>
      </c:choose>
  
  </div>
</nav>
<main class="container-fluid">