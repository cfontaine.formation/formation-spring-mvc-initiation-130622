<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:import url="header.jsp">
    <c:param name="titre" value="Erreur"/>
</c:import>

<h1 class="text-center text-primary display-1 my-5"><c:out value="${codeErreur}"/></h1>
<h2 class="text-center"><c:out value="${msgErreur}"/></h2>

<c:import url="footer.jsp"/>